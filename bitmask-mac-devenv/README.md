## Ansible playbook to setup Dev env for Bitmask VPN development

### Playbook organisation

```
├── ansible.cfg         # Ansible config inventory, pipelining
├── inventory.yml       # Example inventory file
├── requirements.yml    # Collection and roles the playbook uses
├── site.yml            # Playbook
└── vars
    └── main.yml        # variables for names, URLs or versions of dependencies
```

It currently installs Xcode, Homebrew and Qt Installer FW on the host apart from the various development tools installed from Homebrew

All the dependencies from homebrew are defined using the `homebrew_installed_packages` variable, all of the versions, URLs and names of packages
are extracted into variables in `vars/main.yml`

For installing Homebrew and packages from it we make use of the `geerlingguy.mac.homebrew` role from the [mac ansible collection.](https://galaxy.ansible.com/geerlingguy/mac)

The included `inventory` file is just an example file for easy testing during development

**Note:** To install Xcode we need the Xcode xip file, download the 11.7 version of Xcode from developers.apple.com and place it in your `$HOME/Downloads` folder.

### How to run the playbook

Make sure you have a valid inventory file, update the provided `inventory.yml` file with your VM or remote host's IP address and run:

```
$ ansible-playbook site.yml -u <user_in_target_manchine> --key-file <path_to_ssh_key> --ask-become-pass
```
Or to target the localhost run:

```
$ ansible-playbook --connection=local --ask-become-pass site.yml
```

